# 1. Use tini as a lightweight process manager

Date: 2024-06-18

## Status

Accepted

## Context

GitLab 15.0 [moved all processes to PID 1](https://gitlab.com/gitlab-org/charts/gitlab/-/issues/3249) to ensure
that they receive SIGTERM when the container shuts down.

However, PID 1 has a special responsibility as described in
https://blog.phusion.nl/2015/01/20/docker-and-the-pid-1-zombie-reaping-problem/:

> Its task is to "adopt" orphaned child processes (again, this is the
> actual technical term). This means that the init process becomes the
> parent of such processes, even though those processes were never
> created directly by the init process.

This was brought to our attention by Puma v6.4.1, which was deployed a
with [a change that attempts to reap child proceses for PID 1](https://github.com/puma/puma/pull/3255).
Unfortunately, there is a [Ruby bug present in 3.1 and 3.2](https://bugs.ruby-lang.org/issues/20490)
that prevents `Process.wait2(-1)` from reaping returning reaped child
processes on PID 1 when `Process.detach` is run. `Process.detach` is
used to spawn a separate Prometheus metrics server.

As a result, the lack of PID reaping change caused an [incident on `websockets` nodes](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17372).
This incident would have likely been avoided had a process manager been running as PID 1.

### Why are there so many zombie processes?

It appears that both `webservice` and `sidekiq` nodes use the `gpgme`
Ruby gem, which shells out to `gpgconf` and `gpg` when a user manages
GPG keys:

```ruby
irb(main):001:0> GPGME::Engine.info
=>
[#<GPGME::EngineInfo:0x00007fa9a3a698f8 @file_name="/opt/gitlab/embedded/bin/gpg", @protocol=0, @req_version="1.4.0", @version="2.2.41">,
 #<GPGME::EngineInfo:0x00007fa9a3a69740 @file_name="/opt/gitlab/embedded/bin/gpgsm", @protocol=1, @req_version="2.0.4", @version="2.2.41">,
 #<GPGME::EngineInfo:0x00007fa9a3a69628 @file_name="/opt/gitlab/embedded/bin/gpgconf", @protocol=2, @req_version="2.0.4", @version="2.2.41">,
 #<GPGME::EngineInfo:0x00007fa9a3a69560 @file_name="/var/opt/gitlab/.gnupg/S.gpg-agent", @home_dir="!GPG_AGENT", @protocol=3, @req_version="1.0.0", @version="1.0.0">,
 #<GPGME::EngineInfo:0x00007fa9a3a693f8 @file_name="/var/opt/gitlab/.gnupg/S.uiserver", @protocol=5, @req_version="1.0.0", @version="1.0.0">,
 #<GPGME::EngineInfo:0x00007fa9a3a692e0 @file_name="/nonexistent", @protocol=6, @req_version="1.0.0", @version="1.0.0">]
```

For example:

```ruby
crypto = GPGME::Crypto.new
options = {:recipients => 'A1B2C3D4'}
plaintext = GPGME::Data.new(File.open(Rails.root.join('Gemfile')))
data = crypto.encrypt plaintext, options
```

The encrypt method creates a GPGME context and calls [`gpgme_op_encrypt`](https://pyme.sourceforge.net/doc/gpgme/Encrypting-a-Plaintext.html).
This will launch the GPG engine and execute a process.

Without a process manager, if the `puma` or `sidekiq` parent process dies before the GPG processes terminate,
zombie processes will accumulate.

### Candidates

There are a number of candidates that could be used for a process manager:

1. [`tini`](https://github.com/krallin/tini)
1. [`dumb-init`](https://github.com/Yelp/dumb-init)
1. [`runit`](https://smarden.org/runit/)

Both `tini` and `dumb-init` are lightweight process managers and require
no extra configuration to run. The latest release for `tini` is dated
2020-04-19, while the latest release for `dumb-init` is dated
2021-02-01.

Docker uses a fork of `tini` in `docker-init`. The GitLab Runner ships
with `tini` in its RedHat-based helper.

Omnibus GitLab and the GitLab Development Kit use `runit`, but it requires
extra configuration files to manage.

## Decision

Use `tini` for now since it solves the zombie process problem and
requires minimal effort to get working.

## Consequences

For Debian and UBI-based images, `tini` will be shipped in the
`gitlab-base` container. This will initially be off by default and
enabled via an environment flag.
