ARG CI_REGISTRY_IMAGE="registry.gitlab.com/gitlab-org/build/cng"
ARG TAG="master"

ARG FROM_IMAGE="$CI_REGISTRY_IMAGE/gitlab-go"
ARG GO_TAG="${TAG}"
ARG GITLAB_BASE_IMAGE="${CI_REGISTRY_IMAGE}/gitlab-base:${TAG}"

FROM --platform=${TARGETPLATFORM} ${FROM_IMAGE}:${GO_TAG} AS builder

ARG GITLAB_PAGES_VERSION=v1.30.0
ARG GITLAB_NAMESPACE="gitlab-org"
ARG CI_API_V4_URL="https://gitlab.com/api/v4"
# the token is used in security and build mirrors of the project
ARG FETCH_ARTIFACTS_PAT

ENV GOTOOLCHAIN=local

WORKDIR /tmp/build

RUN apt-get update
RUN apt-get install -y --no-install-recommends \
  make
RUN curl -f --retry 6 --header "PRIVATE-TOKEN: ${FETCH_ARTIFACTS_PAT}" \
  "${CI_API_V4_URL}/projects/${GITLAB_NAMESPACE}%2Fgitlab-pages/repository/archive.tar.bz2?sha=${GITLAB_PAGES_VERSION}" \
  | tar xj --strip-components=1
RUN make 'gitlab-pages' "LAST_TAG=v${GITLAB_PAGES_VERSION}" "VERSION=${GITLAB_PAGES_VERSION}"
RUN install -m +x ./gitlab-pages /usr/local/bin/gitlab-pages

## FINAL IMAGE ##

FROM --platform=${TARGETPLATFORM} ${GITLAB_BASE_IMAGE} AS final

ARG GITLAB_USER=git
ARG CONFIG_DIRECTORY=/etc/gitlab-pages
ARG DATA_DIRECTORY=/srv/gitlab-pages

# create gitlab user, necessary directories and set ownership
RUN adduser --disabled-password --gecos 'GitLab' ${GITLAB_USER} \
    && mkdir -p ${CONFIG_DIRECTORY} ${DATA_DIRECTORY} /var/log/gitlab \
    && chown -R ${GITLAB_USER}:${GITLAB_USER} ${CONFIG_DIRECTORY} ${DATA_DIRECTORY} /var/log/gitlab

COPY --from=builder /usr/local/bin/gitlab-pages /bin/gitlab-pages

ENV CONFIG_TEMPLATE_DIRECTORY=${CONFIG_DIRECTORY}

USER $GITLAB_USER:$GITLAB_USER

COPY scripts/ /scripts/

CMD ["/scripts/start-pages"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 \
CMD /scripts/healthcheck
