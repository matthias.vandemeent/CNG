require 'base64'
require 'fileutils'
require 'openssl'
require 'time'
require 'uri'
require 'yaml'

class AzureBackupUtil
  class AzureConfigInvalidError < StandardError
    def initialize(config_file, key)
      super "Azure config file (#{config_file}) needs a valid #{key}"
    end
  end

  def initialize(azure_config_file = '/etc/gitlab/objectstorage/azure_config',
                 token_expire_s = 12 * 60 * 60,
                 sas_token_file = '/tmp/azure_sas_token')
    @config = load_config azure_config_file
    @sas_token_file = sas_token_file
    @token_expire_s = token_expire_s
    @current_token = {}
  end

  def url
    "https://#{@config['azure_storage_account_name'].to_s.strip}.#{@config['azure_storage_domain'].to_s.strip}"
  end

  def static_credentials?
    @config['azure_storage_account_name'] && @config['azure_storage_access_key'] &&
      !@config['azure_storage_access_key'].empty?
  end

  # https://azure.github.io/kubelogin/concepts/login-modes/workloadidentity.html
  def workload_identity?
    ENV['AZURE_TENANT_ID'] && ENV['AZURE_CLIENT_ID'] && ENV['AZURE_FEDERATED_TOKEN_FILE']
  end

  def azure_url(object_name)
    base_url = "#{url}/#{object_name}"

    return base_url unless static_credentials?

    "#{base_url}?#{sas_token}"
  end

  def azcopy_env
    return {} if static_credentials?

    if workload_identity?
      { 'AZCOPY_AUTO_LOGIN_TYPE' => 'workload' }
    else
      # Support the ability to use managed identities on a VM
      { 'AZCOPY_AUTO_LOGIN_TYPE' => 'MSI' }
    end
  end

  private

  def sas_token
    if @current_token['token'].nil? ||
       @current_token['expiry'].nil? ||
       @current_token['expiry'] - (@token_expire_s / 2) < Time.now
      # refresh token
      start = Time.now - 10 * 60
      expiry = Time.now + @token_expire_s
      @current_token = generate_sas(@config['azure_storage_access_key'], @config['azure_storage_account_name'],
                                    start, expiry)
    end
    @current_token['token']
  end

  def load_config(config_file)
    conf = YAML.safe_load_file config_file
    mandatory_keys = %w(azure_storage_account_name)
    mandatory_keys.each do |key|
      raise AzureConfigInvalidError.new(config_file, key) if conf[key].nil?
    end

    conf['azure_storage_domain'] = 'blob.core.windows.net' if conf['azure_storage_domain'].nil?
    conf
  end

  def generate_sas(access_key, account, start, expiry)
    permissions = 'rawdl'  # read, access, write, delete, list
    protocol = 'https'     # allow https only
    service = 'b'          # blob storage
    resources = 'co'       # container + object level
    version = '2018-11-09' # API Version
    ip_range = ''          # Allowed IPs

    # item order and empty items/lines matter
    to_sign = [
      account,
      permissions,
      service,
      resources,
      start.utc.iso8601,
      expiry.utc.iso8601,
      ip_range,
      protocol,
      version,
      ""
    ].join("\n")

    sig = OpenSSL::HMAC.digest('sha256', Base64.decode64(access_key), to_sign)
    sig = Base64.strict_encode64(sig)
    sig = URI.encode_www_form_component(sig)
    token = "sv=#{version}&ss=#{service}&srt=#{resources}&sp=#{permissions}"\
      "&se=#{expiry.utc.iso8601}&st=#{start.utc.iso8601}&spr=#{protocol}&sig=#{sig}"
    { 'token' => token, 'start' => start, 'expiry' => expiry }
  end
end
